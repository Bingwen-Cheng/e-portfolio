import React from 'react';
import SideNav from '../Components/sideNav';
import {ProfileConfig} from '../../Configs/ProfileConfig'
import fp from 'lodash/fp';
import { Switch, Route } from 'react-router-dom';
import Projects from './Projects/projects';
import Passion from './Passion/passion';
import BasicInfo from './BasicInfo/basicInfo';


const Displaying = () => {

  const sections = fp.flow(
    fp.mapValues('label'),
    fp.toPairs
    )(ProfileConfig)


  return <div id = 'displaying'>
    <SideNav sections = {sections} />
    <div className='display-content'>
    <Switch>
        <Route path='/basicInfo'>
          <BasicInfo info={ProfileConfig.basicInfo.data}/>
        </Route>
      </Switch>
      <Switch>
        <Route path='/projects'>
          <Projects projects={ProfileConfig.projects.data}/>
        </Route>
      </Switch>
      <Switch>
        <Route path='/passions'>
          <Passion images={ProfileConfig.passions.data}/>
        </Route>
      </Switch>
    </div>

  </div>
}

export default Displaying