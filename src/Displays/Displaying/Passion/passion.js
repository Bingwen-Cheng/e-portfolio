import React, { useState } from 'react';
import ActiveGallery from './gallery';
import fp from 'lodash/fp';


const Passion = ({images}) => {

  const groupedImages = {
    All:images,
    ...fp.groupBy('type')(images),
  }

  const navList = fp.keys(groupedImages)
  const [activeImages, setActiveImages] = useState(images)



  return <div id='passion'>
    <div id='top-nav'>
      {navList.map((item, index) => 
        <p 
          key={index}
          className='nav-item'
          style={{animationDelay:`${0.5 + 0.1 * index}s`}} 
          onClick={()=>setActiveImages(groupedImages[item])}
        >
          {item}
        </p>
      )}
    </div>
    <div id='gallery-background'>
      <ActiveGallery photos={activeImages} />
    </div>

  </div>
}


export default Passion