import React, { useState, useEffect } from 'react';
import Project from './project';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';


const Projects = ({projects}) => {
  const [activeProject, setActiveProject] = useState(projects[0])
  
  const [isNarrow, setIsNarrow] = useState(
    window.innerWidth > 710 ? false:true
  )
  const projectNum = projects.length
  const handleSelect = (project) => () => {
    setActiveProject(project)
  }


  const nextProject = () => {
    if(activeProject.index === projectNum-1){
      setActiveProject(projects[0])
    }else{
      setActiveProject(projects[activeProject.index+1])
    }

  }

  const previousProject = () => {
    if(activeProject.index === 0) {
      setActiveProject(projects[projectNum-1])
    }else{
      setActiveProject(projects[activeProject.index-1])
    }
  }

  useEffect(
    () => {
      const listener = window.addEventListener('resize',function(){
        setIsNarrow(
          window.innerWidth > 710 ? false:true
        )
      })

      return () => {
        window.removeEventListener('resize', listener)
      }
    },
    [setIsNarrow]
  )


  return <div id = 'projects'>
    {
      isNarrow?
      <div id='narrow-top-nav'>
        <ChevronLeftIcon className='nav-left' onClick={previousProject}/>
          <p>{activeProject.index+1}/{projectNum}</p>
        <ChevronRightIcon className='nav-right' onClick={nextProject}/> 
      </div>:
      <div id='top-nav'>
        {
          projects.map(
            (project, index) => {
              return <p 
                style={{animationDelay:`${0.5 + 0.1 * index}s`}} 
                key={index} 
                className='nav-item' 
                onClick={handleSelect(project)}>
                  {project.name}
                </p>
            }
          )
        }
      </div>
    }
    {
      activeProject && <Project project={activeProject} />
    }
  </div>
}


export default Projects