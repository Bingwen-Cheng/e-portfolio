import React, { useState } from 'react';
import fp from 'lodash/fp';

const Project = ({project}) => {
  const {name, intro, image, tags, link} = project
  const [activeDetail, setActivePortal] = useState('intro')
  return<div key={name} id='project'>
    <div id='project-background'>
      <div id='project-canvas' style={{backgroundImage:`url(${image})`}}>
        <div id='project-img' />
        <div id = 'project-detail'>
          <div className='detail-tab'>
            <p onClick={()=>setActivePortal('intro')}>Summary</p>
            <p onClick={()=>setActivePortal('tags')}>Technologies</p>
          </div>
          <div id='detail-panel'>
            {activeDetail==='intro'?<p id='detail-intro'>{intro}</p>:null}
            {
              activeDetail==='tags'?
                <div id='detail-tags'>
                  {fp.map(tag=><p>{tag}</p>)(tags)}
                </div>:null
            }
          </div>
        </div>
        <div id='project-intro'>
          <div id='project-name'><h1>{name}</h1></div>
          <a id='project-link' href={link} rel="noopener noreferrer" target='_blank'>
            <h4>CHECK IT OUT</h4>
          </a>
        </div>
      </div>
    </div>
  </div>
}


export default Project