import React from 'react';


const BasicInfo = ({info}) => {
  const{name,contact,intro,image} = info
  return <div id='basic-info'>
    <div id='name-contact'>
      <div id='name'><h1>{name}</h1></div>
      <div id='contact'>
        <p>{contact.phone}</p>
        <a href={`mailto:${contact.email}?subject=MAIL FROM PORTFOLIAO`}><p>{contact.email}</p></a>
      </div>
    </div>
    <div id='photo-detail'>
      <div id='photo' style={{backgroundImage:`url(${image.photo})`}} />
      <div id='detail'>
        <div id ='detail-background'><div id='detail-animation'></div>
        </div>
        <div id='detail-content'>
          <div id='summary'>
            <h3 className='detail-heading'>summary</h3>
            <p id='summary-panel'>{intro.summary}</p>
          </div>
          <div id='education'>
            <h3 className='detail-heading'>Education</h3>
            <p id='education-panel'>{intro.education}</p>
          </div>
          <div id='skills'>
            <h3 className='detail-heading'>Skills</h3>
            <div id='skills-panel'>{intro.skills.map((skill,index)=><p key={index}>{skill}</p>)}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
}


export default BasicInfo