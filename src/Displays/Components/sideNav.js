import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import wendyCheng_resume from '../../resource/wendyCheng_resume.pdf';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';



const SideNav = ({sections}) => {
  const [isFullWidth, setIsFullWidth] = useState(false);

  return <div id='side-nav' className={isFullWidth ? 'full-width':'narrow-width'}>
    <ChevronRightIcon className='right-icon' onClick={()=>setIsFullWidth(true)} />
    <ChevronLeftIcon className='left-icon' onClick={()=>setIsFullWidth(false)}/>
    <div className = 'side-nav-item'>
      {
        sections.map(
          (section, index) => (section[0]==='resume' ?
            <Link key={index} to={wendyCheng_resume} target='_blank' download><h3>{section[1]}</h3></Link> :
            <Link key={index} to={section[0]}><h3 onClick={()=>setIsFullWidth(false)}>{section[1]}</h3></Link>
          )
        )
      }
    </div>

  </div>
}

export default SideNav