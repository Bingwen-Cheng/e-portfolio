import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
       } from 'react-router-dom';
       import './App.css';
import GoogleFontLoader from 'react-google-font-loader';
import Welcoming from './Displays/Components/Welcoming';
import Displaying from './Displays/Displaying/displaying'



const App = () => {

  
  return (
    <Router>
      <div>
        <GoogleFontLoader 
                fonts={[
                  { font: 'Dancing Script' },
                  { font: 'Bellota'},
                  { font: 'Kanit'}
                ]}
              />
        <Switch>
          <Route path='/'>
            <div>
              <Welcoming />
              <Displaying />
            </div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
