import photo from '../resource/photo.jpg';
import Icon_1 from '../resource/Icon-1.jpg';
import Icon_2 from '../resource/Icon-2.jpg';
import Icon_3 from '../resource/Icon-3.jpg';
import Comic_UserStory_1 from '../resource/Comic-UserStory-1.jpg';
import Comic_UserStory_2 from '../resource/Comic-UserStory-2.jpg';
import Comic_UserStory_3 from '../resource/Comic-UserStory-3.jpg';
import Illustration_1 from '../resource/Illustration-1.jpg';
import Illustration_2 from '../resource/Illustration-2.jpg';
import Illustration_3 from '../resource/Illustration-3.jpg';
import Illustration_4 from '../resource/Illustration-4.jpg';
import Sketch_1 from '../resource/Sketch-Anhui.jpg';
import Sketch_2 from '../resource/Sketch-Fenghuang.jpg';
import Sketch_3 from '../resource/Sketch-Kings_College_Cambridge.jpg';
import Sketch_4 from '../resource/Sketch-Xiamen.jpg';
import Sketch_5 from '../resource/Sketch-Roby.jpg';
import WallPaper_1 from '../resource/WallPaper-1.jpg';
import WallPaper_2 from '../resource/WallPaper-2.jpg';
import WallPaper_3 from '../resource/WallPaper-3.jpg';
import wendyCheng_Resume from '../resource/wendyCheng_resume.pdf';
import Project_upAcademy from '../resource/Project-upAcademy.png';
import Project_uniOL from '../resource/Project-uniOL.png';
import Project_oneClean from '../resource/Project-oneclean.png';
import Project_ePortfolio from '../resource/Project-ePortfolio.png';
import Project_mymemory from '../resource/Project-myMermoryMaps.png';
import Project_PostalImprovemnt from '../resource/Project-PostalImprovement.png';

export const ProfileConfig = {
  basicInfo:{
    label: 'Meet Me',
    data: {
      name: 'Wendy Cheng',
      contact:{
        phone:'0416940912',
        email:'bingwencheng.wendy@gmail.com',
      },
      intro: {
        summary:'A Curious and enthusiastic Web Developer, Front End Developer, WordPress Developer and FreeLancer',
        education: 'Master of Computing, Australian National University',
        skills:[
          'JavaScript (ES6) ',
          'HTML/CSS ',
          'Typescript ', 
          'MySQL ', 
          'WordPress ', 
          'React.js ', 
          'Meteor.js ', 
          'NodeJS (Express) ', 
          '.NET ',
          'Bootstrap ', 
          'Material UI ', 
          'Ant Design ', 
          'Redux ', 
          'Lodash ', 
          'Webpack ', 
          'JEST ', 
          'Photoshop ', 
          'Illustrator ', 
          'Procreate ', 
          'Git ', 
          'JIRA ',
          'Agile Development (Scrum) ',
          'Object-Oriented Programming (OOP) ',
          'Functional Programming '
        ]
      },
      image: {
        photo: photo,
        painting:''
      }
    }
  },

  projects: {
    label: 'Projects',
    data: [
      { 
        index:0,
        name:'UP Academy',
        intro:`UP Academy is a education agency located in ANU campus. To promote their new project courses, they decide to update their website. This new website can display company promotional videos, courses and recruitment information. It is expected that the background management system will be done in the near future`,
        tags:['React.js', '.NET', 'TypeScript', 'C#','Axios','React Player', 'Ant Design'],
        image: Project_upAcademy,
        link:'https://www.upacademy.com.au/'
      },
      {
        index:1,
        name:'E-Portfolio',
        intro:'During my job hunting, I realized that I really need a portfolio. Then I thought, why not make it myself, so this repository takes my information and generate a descently styled e-portfolio for me.',
        tags:['React.js', 'JavaScript','Material UI','react-anime','react-images'],
        image: Project_ePortfolio,
        link:''
      },
      {
        index:2,
        name:'One Clean',
        intro:'One Clean is a local business which provides cleaning services. The owner of the company asked me to design and build a website to display their services and quote online.',
        tags:['Meteor.js', 'Ant Design','SendGrid'],
        image: Project_oneClean,
        link:'https://www.onecleancanberra.com/'
      },
      {
        index:3,
        name:'My Memory',
        intro:'This is an online dictionary which provides different modes for users to search and memory words. The learning mode can return a series of related words after searching for one word. And the dictation mode can let users do dictation. ',
        tags:['Meteor.js', 'Bootstrap', 'Text to Speech'],
        image: Project_mymemory,
        link:'https://www.mymemorymaps.com/'
      },
      {
        index:4,
        name:'UniOL',
        intro:'This is a website where you can organise knowledge in a structured way and share with others. You can also view knowledge structures shared by others. In this platform, you can meet people who have the same interest or enrolled in the same course with you, make a group and discuss together.',
        tags:['Meteor.js', 'Ant-Design', 'MongoDB'],
        image: Project_uniOL,
        link:'https://www.uni-ol.com/'
      },
      {
        index:5,
        name:'Postal Improvement',
        intro:'In an ANU internship course, Accenture asked my team to investigate and provide technical improvements for Australian posting system. In this agile project, I mainly participated in front-end development and UI design.',
        tags:['React.js', 'Redux', 'MDN','React Hooks', 'SpringBoot'],
        image: Project_PostalImprovemnt,
        link:'https://www.youtube.com/watch?v=pf3fob19K0s'
      },
    ]
  },
  
  passions: {
    label: 'Gallery',
    data: [
      {
        type:'Icon',
        src:Icon_1,
        width:1.5,
        height:1
      },
      {
        type:'Icon',
        src:Icon_2,
        width:1.5,
        height:1
      },
      {
        type:'Icon',
        src:Icon_3,
        width:1.5,
        height:1
      },
      {
        type:'Wall Paper',
        src:WallPaper_1,
        width:1.5,
        height:2.5
      },
      {
        type:'Wall Paper',
        src:WallPaper_2,
        width:1.5,
        height:1
      },
      {
        type:'Wall Paper',
        src:WallPaper_3,
        width:1.5,
        height:2.5
      },
      {
        type:'Illustration',
        src:Illustration_1,
        width:1.5,
        height:1
      },
      {
        type:'Illustration',
        src:Illustration_2,
        width:1.5,
        height:1
      },
      {
        type:'Illustration',
        src:Illustration_3,
        width:1.5,
        height:1.5
      },
      {
        type:'Illustration',
        src:Illustration_4,
        width:1.5,
        height:2
      },
      {
        type:'Comic',
        src:Comic_UserStory_1,
        width:1.5,
        height:1
      },
      {
        type:'Comic',
        src:Comic_UserStory_2,
        width:1.5,
        height:1
      },
      {
        type:'Comic',
        src:Comic_UserStory_3,
        width:1.5,
        height:1
      },
      {
        type:'Sketch',
        src:Sketch_1,
        width:1.5,
        height:2
      },
      {
        type:'Sketch',
        src:Sketch_2,
        width:1.5,
        height:1
      },
      {
        type:'Sketch',
        src:Sketch_3,
        width:1.5,
        height:1
      },
      {
        type:'Sketch',
        src:Sketch_4,
        width:1.5,
        height:2
      },
      {
        type:'Sketch',
        src:Sketch_5,
        width:1.5,
        height:2
      },
      
    ]
  },

  resume: {
    label:'Resume',
    data:wendyCheng_Resume
  }
}